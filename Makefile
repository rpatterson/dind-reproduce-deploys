# Reproduce hosted deployments in nested Docker (DinD)

### Defensive settings for make:
#     https://tech.davis-hansson.com/p/make/
SHELL := bash
.ONESHELL:
.SHELLFLAGS:=-xeu -o pipefail -O inherit_errexit -c
.SILENT:
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules


# Top-level targets

.PHONY: all
all: var/log/docker-compose-build.log foo-host-corge/.env

.PHONY: run
run: all
	docker-compose exec -T "foo-host-corge" docker-compose up -d
	docker-compose exec -T "foo-host-grault" docker-compose up -d
	sleep 1
	docker-compose exec -T "foo-host-corge" docker-compose ps
	docker-compose exec -T "foo-host-grault" docker-compose ps

.PHONY: test
test: run
# Demonstrate that deployment host network topology and hostnames are reproduced
	docker-compose exec foo-host-corge \
	    docker-compose exec foo-app nc -vz redis 6379
	docker-compose exec foo-host-grault \
	    docker-compose exec foo-app nc -vz postgres 5432
# Demonstrate that deployment host filesystem paths and data are reproduced
	sudo rm -rfv ./var/lib/foo-data/*
	ls -al "./var/lib/foo-data/"
	docker-compose exec foo-host-corge \
	    ls -al "/home/ec2-user/foo-data/"
	docker-compose exec foo-host-grault \
	    ls -al "/home/ec2-user/foo-data/"
	docker-compose exec foo-host-corge docker-compose exec foo-app \
	    ls -al "/srv/foo-data/"
	docker-compose exec foo-host-grault docker-compose exec foo-app \
	    ls -al "/srv/foo-data/"
	docker-compose exec foo-host-grault docker-compose exec foo-app \
	    test ! -e "/srv/foo-data/bar.txt"
	sudo touch "./var/lib/foo-data/bar.txt"
	docker-compose exec foo-host-corge docker-compose exec foo-app \
	    ls -al "/srv/foo-data/"
	docker-compose exec foo-host-grault docker-compose exec foo-app \
	    ls -al "/srv/foo-data/"
	docker-compose exec foo-host-grault docker-compose exec foo-app \
	    test -e "/srv/foo-data/bar.txt"

.PHONY: clean
clean:
	docker-compose down --rmi local -v
	sudo rm -rf "./var/lib/"
	mkdir -pv "./var/log/backups/"
	test ! -e "./var/log/docker-compose-build.log" ||
	    mv --backup=numbered -v "./var/log/docker-compose-build.log" \
	        "./var/log/backups/"
	test ! -e "./foo-host-corge/.env" ||
	    mv --backup=numbered -v "./foo-host-corge/.env" \
	        "./var/log/backups/"

# Real targets

var/log/docker-compose-build.log: foo-app/Dockerfile Dockerfile docker-compose.yml
	mkdir -pv "./$(dir $(@))/"
	sudo docker-compose build --pull | tee -a "./$(@)"
# Wait for the nested deployment host `# dorckerd` daemons to become available
	docker-compose up -d
	for host in foo-host-corge foo-host-grault
	do
	    timeout --foreground -v 20 $(SHELL) $(.SHELLFLAGS) "\
		while ! docker-compose exec -T "$${host}" docker ps; do sleep 0.1; done"
# Load the built image into the deployment host Docker daemons
	    docker save "bar-company/foo-app:dev" |
		docker-compose exec -T "$${host}" docker load
	done

foo-host-corge/.env:
	echo "POSTGRES_PASSWORD=$$(apg -M NCL -n 1)" >"./$(@)"
