$ make test
+ docker-compose exec -T foo-host-corge docker-compose up -d
foo-host-corge_foo-app_1 is up-to-date
foo-host-corge_postgres_1 is up-to-date
+ docker-compose exec -T foo-host-grault docker-compose up -d
foo-host-grault_redis_1 is up-to-date
foo-host-grault_foo-app_1 is up-to-date
+ sleep 1
+ docker-compose exec -T foo-host-corge docker-compose ps
          Name                         Command               State           Ports
-------------------------------------------------------------------------------------------
foo-host-corge_foo-app_1    python -m http.server --di ...   Up      0.0.0.0:80->80/tcp
foo-host-corge_postgres_1   docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp
+ docker-compose exec -T foo-host-grault docker-compose ps
          Name                         Command               State           Ports
-------------------------------------------------------------------------------------------
foo-host-grault_foo-app_1   python -m http.server --di ...   Up      0.0.0.0:80->80/tcp
foo-host-grault_redis_1     docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp
+ docker-compose exec foo-host-corge docker-compose exec foo-app nc -vz redis 6379
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 10.81.82.101:6379.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
+ docker-compose exec foo-host-grault docker-compose exec foo-app nc -vz postgres 5432
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 10.81.82.100:5432.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
+ sudo rm -rfv './var/lib/foo-data/*'
+ ls -al ./var/lib/foo-data/
total 8
drwxr-xr-x 2 root root 4096 Apr  5 08:49 .
drwxr-xr-x 5 root root 4096 Apr  5 08:45 ..
+ docker-compose exec foo-host-corge ls -al /home/ec2-user/foo-data/
total 8
drwxr-xr-x    2 root     root          4096 Apr  5 15:49 .
drwxr-sr-x    1 ec2-user ec2-user      4096 Apr  5 15:45 ..
+ docker-compose exec foo-host-grault ls -al /home/ec2-user/foo-data/
total 8
drwxr-xr-x    2 root     root          4096 Apr  5 15:49 .
drwxr-sr-x    1 ec2-user ec2-user      4096 Apr  5 15:45 ..
+ docker-compose exec foo-host-corge docker-compose exec foo-app ls -al /srv/foo-data/
total 8
drwxr-xr-x 2 root root 4096 Apr  5 15:49 .
drwxr-xr-x 1 root root 4096 Apr  5 15:45 ..
+ docker-compose exec foo-host-grault docker-compose exec foo-app ls -al /srv/foo-data/
total 8
drwxr-xr-x 2 root root 4096 Apr  5 15:49 .
drwxr-xr-x 1 root root 4096 Apr  5 15:45 ..
+ docker-compose exec foo-host-grault docker-compose exec foo-app test '!' -e /srv/foo-data/bar.txt
+ sudo touch ./var/lib/foo-data/bar.txt
+ docker-compose exec foo-host-corge docker-compose exec foo-app ls -al /srv/foo-data/
total 8
drwxr-xr-x 2 root root 4096 Apr  5 15:50 .
drwxr-xr-x 1 root root 4096 Apr  5 15:45 ..
-rw-r--r-- 1 root root    0 Apr  5 15:50 bar.txt
+ docker-compose exec foo-host-grault docker-compose exec foo-app ls -al /srv/foo-data/
total 8
drwxr-xr-x 2 root root 4096 Apr  5 15:50 .
drwxr-xr-x 1 root root 4096 Apr  5 15:45 ..
-rw-r--r-- 1 root root    0 Apr  5 15:50 bar.txt
+ docker-compose exec foo-host-grault docker-compose exec foo-app test -e /srv/foo-data/bar.txt
