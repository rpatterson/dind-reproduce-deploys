#######################################################
Reproduce Hosted Deployments with Docker-in-Docker Demo
#######################################################
Demonstrate how to reproduce hosted deployments locally using Docker-in-Docker.
*******************************************************************************

Use ``$ make run`` to run the demo DinD deployment hosts with running nested containers.
See `the blog post`_ for further discussion and details.

.. _`the blog post`: https://www.rpatterson.net/blog/dind-reproduce-deploys/

.. meta::
   :description: Debugging deployment issues locally by falling down the
                 Docker-in-Docker (DND) rabbit hole.
   :keywords: Docker, DinD, docker-compose
