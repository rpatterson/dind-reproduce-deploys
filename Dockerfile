FROM docker:dind
# Defensive shell settings, avoid silent failures
SHELL ["/bin/ash", "-xeu", "-c"]

# Install required OS host packages
RUN \
        apk --no-cache add 'py-pip' 'python3-dev' 'libffi-dev' 'openssl-dev' 'gcc' \
        'libc-dev' 'rust' 'cargo' 'make' && \
        pip3 install 'docker-compose'

# Duplicate the AWS EC2 user
RUN adduser --disabled-password --gecos 'EC2 User,,,' ec2-user
