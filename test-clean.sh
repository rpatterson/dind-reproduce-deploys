#!/bin/bash -xeu
#
# Clone the repository locally, perform a clean build, and test

set -o pipefail


main() {
    # Extract the details we need from the existing checkout
    remote_url="${1:-$PWD}"
    branch=$(cd "$remote_url" && git branch --show-current)

    # Clone the local checkout into a clean checkout
    tmp_dir=$(mktemp -d "$(realpath "${remote_url}").test-clean.XXXXXXXXXX")
    git clone --depth 1 --reference "${remote_url}" -b "${branch}" \
        "${remote_url}" "${tmp_dir}"
    cd "${tmp_dir}"
    git config --local "remote.pushDefault" "origin"
    git submodule status | sed -nE 's/^-[^ ]+ (.+)/\1/p' | while read; do
	git submodule update --init --recursive \
	    --reference "${remote_url}/${REPLY}" "${REPLY}"; done

    # Do a clean build in the temporary checkout, run and test
    make test
}

main "$@"
